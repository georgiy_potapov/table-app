import React from 'react'
import uuid from 'uuid/v1';

export default class Row extends React.Component {

    render() {
        return (
            <tr onClick={this.getDataForVisual}>
                {this.createRow(this.props.elem)}
            </tr>
        );
    }

    createRow(elem) {
        let subArrayFromElem = Object.values(elem).slice(0, 5);
        return subArrayFromElem.map( elem => <td key={uuid()}>{elem}</td> );
    }

    getDataForVisual = () => {
        this.props.rowInfoBoxHandler(this.props.elem);
    }
}