import React from 'react'

import Row from './Row'
import Loader from './Loader'
import RowInfoViewer from './RowInfoViewer'
import FilterPlugin from './FilterPlugin'
import TableHead from './TableHead'
import Constans from './Constans.js'

import uuid from 'uuid/v1';

export default class App extends React.Component {

    constructor() {
        super();

        this.state = {  loadedData : null, 
                        prevButtonEnabled : 'disabled', 
                        nextButtonEnabled : 'disabled', 
                        beginIndex : 0, 
                        textwhileLoadingData : '', 
                        currentPageNumber : 1, 
                        rowInfoBox : null,
                        filteredText : '',
                        orderBy : { order : Constans.Ordering.BY_ID, direction : Constans.OrderDirection.ASCEND } 
        };
    }

    render() {
        if(this.state.loadedData === null) {        
            return (this.state.textwhileLoadingData !== '') ? <div>{this.state.textwhileLoadingData}</div> : <Loader setLoadedData={this.setLoadedData} />;
        }
        else {
            return(<div>
                        <p>Номер страницы - {this.state.currentPageNumber}</p>
                        <FilterPlugin setFilterText={this.setFilterText} />
                        {this.currentPageView()}
                        <button onClick={this.prevPage} disabled={this.state.prevButtonEnabled}>Назад</button>
                        <button onClick={this.nextPage} disabled={this.state.nextButtonEnabled}>Вперед</button>
                        <RowInfoViewer rowInfoBox={this.state.rowInfoBox} />
                  </div> );
        }
    }

    currentPageView() {
        let endIndex = this.state.beginIndex + Constans.ROWS_PER_PAGE > this.state.loadedData.length ? this.state.loadedData.length : this.state.beginIndex + Constans.ROWS_PER_PAGE; 
        let currentArray = this.state.loadedData.slice(this.state.beginIndex, endIndex);

        if(this.state.filteredText !== '')
            currentArray = this.filtration(currentArray, this.state.filteredText);

        this.ordering(currentArray, this.state.orderBy, );

        let table = currentArray.map( elem => <Row key={uuid()} elem={elem} rowInfoBoxHandler={this.rowInfoBoxHandler} /> );

        return ( <table border={1}>
                    <tbody>
                        <TableHead setOrderBy={this.setOrderBy} />
                        {table}
                    </tbody>
                 </table> );
    }

    nextPage = () => {
        let newBeginIndex = (this.state.beginIndex + Constans.ROWS_PER_PAGE >= this.state.loadedData.length) ? this.state.loadedData.length : this.state.beginIndex + Constans.ROWS_PER_PAGE;
        let nextBtnEnable = (newBeginIndex + Constans.ROWS_PER_PAGE >= this.state.loadedData.length) ? "disabled" : "";
        let newPageNumber = this.state.currentPageNumber + 1;

        this.setState( {prevButtonEnabled : "",
                        rowInfoBox : null,
                        nextButtonEnabled : nextBtnEnable, 
                        currentPageNumber : newPageNumber,
                        beginIndex : newBeginIndex } );

    }

    prevPage = () => {
        let newBeginIndex = (this.state.beginIndex - Constans.ROWS_PER_PAGE <= 0) ? 0 : this.state.beginIndex - Constans.ROWS_PER_PAGE;
        let prevBtnEnable = (newBeginIndex === 0) ? "disabled" : "";
        let newPageNumber = this.state.currentPageNumber - 1;

        this.setState( {prevButtonEnabled : prevBtnEnable, 
                        rowInfoBox : null,
                        nextButtonEnabled : "", 
                        currentPageNumber : newPageNumber,
                        beginIndex : newBeginIndex } );
    }

    setLoadedData = (properties) => {
        const { content, text, nextButton } = properties;
        this.setState( { loadedData : content || null,      
                         textwhileLoadingData : text || '', 
                         nextButtonEnabled : typeof nextButton === undefined ? "disabled" : nextButton
                    } );
    }

    rowInfoBoxHandler = (data) => {
        this.setState( {rowInfoBox : data} );
    }

    setFilterText = (filterText) => {
        this.setState( { filteredText :filterText } );
    }

    setOrderBy = (newOrderBy, newOrderDirection) => {
        this.setState( { orderBy : { order : newOrderBy, direction : newOrderDirection } } );
    }

    filtration( array, filter ) {

        return array.filter( item => {
                for( let subItem of Object.values(item).slice(0,5) ) {
                    if( typeof subItem === 'number' )
                        subItem = subItem.toString();
                    if( subItem.includes(filter) )
                            return true;
                }
                return false;
            } )
    }

    ordering( array, orderBy ) {
        array.sort( (currentElem, nextElem) => {
            if( currentElem[this.state.orderBy.order] < nextElem[this.state.orderBy.order] )
                return -1;
            else if ( currentElem[this.state.orderBy.order] > nextElem[this.state.orderBy.order] )
                return 1;
            
            return 0; } );
        
        if(orderBy.direction === Constans.OrderDirection.DESCEND)
            array = array.reverse();
    }
}