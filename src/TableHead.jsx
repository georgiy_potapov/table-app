import React from 'react'
import Constans from './Constans.js'

export default class TableHead extends React.Component {

    constructor() {
        super();
        this.state = { id : { orderArrow : Constans.Arrows.UP },
                       firstName : { orderArrow : null },
                       lastName : { orderArrow : null },
                       email : { orderArrow : null },
                       phone : {orderArrow : null },
                       currentColIdentStr : Constans.Ordering.BY_ID };
    };

    render() {
        return (       
                <tr>
                    <td ordelcolby={Constans.Ordering.BY_ID} onClick={this.setOrder}>
                        <b>Id {this.state.id.orderArrow}</b>
                    </td>
                    <td ordelcolby={Constans.Ordering.BY_FIRST_NAME} onClick={this.setOrder}>
                        <b>First Name {this.state.firstName.orderArrow}</b>
                    </td>
                    <td ordelcolby={Constans.Ordering.BY_LAST_NAME} onClick={this.setOrder}>
                        <b>Last Name {this.state.lastName.orderArrow}</b>
                    </td>
                    <td ordelcolby={Constans.Ordering.BY_EMAIL} onClick={this.setOrder}>
                        <b>Email {this.state.email.orderArrow}</b>
                    </td>
                    <td ordelcolby={Constans.Ordering.BY_PHONE_NUMBER} onClick={this.setOrder}>
                        <b>Phone Number {this.state.phone.orderArrow}</b>
                    </td>
                </tr> );
    }

   setOrder = (event) => {
        let orderColBy = event.currentTarget.getAttribute("ordelcolby");

        let direction;

        if(orderColBy === this.state.currentColIdentStr) {
            let arrow = ( this.state[orderColBy].orderArrow === Constans.Arrows.DOWN ) ? Constans.Arrows.UP : Constans.Arrows.DOWN;
            this.setState( { [orderColBy] : {orderArrow : arrow} } );
            direction = (arrow === Constans.Arrows.DOWN) ? Constans.OrderDirection.DESCEND : Constans.OrderDirection.ASCEND;
        }
        else {
            let currentOrderByStr = this.state.currentColIdentStr;
            this.setState( { [orderColBy] : { orderArrow : Constans.Arrows.UP },
                             [currentOrderByStr] : { orderArrow : null },
                             currentColIdentStr : orderColBy } );
            direction = Constans.OrderDirection.ASCEND;
        }  
        this.props.setOrderBy(orderColBy, direction);
    }
}