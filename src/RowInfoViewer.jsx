import React from 'react'

export default class RowInfoViewer extends React.Component {

    render() {
        let currentUserData = this.props.rowInfoBox;
        return (
            currentUserData == null ? <p></p> : this.createInfo(currentUserData) 
        );
    }

    createInfo(currentUserData) {       
        return (
            <div>
                <p>Выбран пользователь: <b>{currentUserData.firstName} {currentUserData.lastName}</b></p>
                <p>Oписание: <textarea value={currentUserData.description} cols={55} rows={5} readOnly/></p>
                <p>Адрес проживания: <b>{currentUserData.address.streetAddress}</b></p>
                <p>Город: <b>{currentUserData.address.city}</b></p>
                <p>Провинция: <b>{currentUserData.address.state}</b></p>
                <p>Индекс: <b>{currentUserData.address.zip}</b></p>
            </div>
        );
    }
}