import React from 'react'

export default class FilterPlugin extends React.Component {

    constructor() {
        super();
        this.handleText = this.handleText.bind(this);
    }

    render() {
        return (
            <div>
                <input type="text" placeholder='поиск..' size={40} onChange={this.handleText}/>           
            </div>
        );
    }

    handleText (event) {
        let filterText = event.target.value.trim();
        this.props.setFilterText(filterText);
    }
}