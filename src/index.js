import {render} from 'react-dom'
import App from '../src/App'
import React from 'react'

render(<App />, document.getElementById('root'))