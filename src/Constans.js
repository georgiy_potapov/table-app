export default class Constans {

    static ROWS_PER_PAGE = 25;
    
    static Ordering = {
        BY_ID : 'id',
        BY_FIRST_NAME : 'firstName',
        BY_LAST_NAME : 'lastName',
        BY_EMAIL : 'email',
        BY_PHONE_NUMBER : 'phone'
    }

    static OrderDirection = {
        ASCEND : 1,
        DESCEND : 2
    }

    static Arrows = {
        UP : String.fromCharCode(11014), //unicode arrow up
        DOWN : String.fromCharCode(11015) //unicode arrow down
    }

    static ContentSize = {
        SMALL : 0,
        BIG : 1
    }
}