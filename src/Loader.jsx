import React from 'react'
import Constans from './Constans'

export default class Loader extends React.Component {
    constructor() {
        super();
        this.state = { contentSize : Constans.ContentSize.SMALL };
    }

    render() {
        return (
            <div>               
                <label><input name="dataSize" type="radio" value={Constans.ContentSize.SMALL} id="rbsmall" defaultChecked onChange={this.switchSize} />малый объем</label>              
                <label><input name="dataSize" type="radio" value={Constans.ContentSize.BIG} id="rbbig" onChange={this.switchSize} />большой объем</label> 
                <button onClick={this.loadingData} >Получить</button>
            </div>
        );
    }

    loadingData = () => {

        const bigData = 'http://www.filltext.com/?rows=1000&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}';
        const smallData = 'http://www.filltext.com/?rows=32&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}';

        this.props.setLoadedData( { text: 'Загружаем данные...' } );

        let url = this.state.contentSize === Constans.ContentSize.SMALL ? smallData : bigData;

        fetch(url)
        .then( response => response.json() )
        .then( data => { 
            this.props.setLoadedData( { content : data, nextButton : (data.length > Constans.ROWS_PER_PAGE) ? "" : "disabled" } );
         } )
        .catch(error => { this.props.setLoadedData({ text : error }) } );
    }

    switchSize = (e) => {
        this.setState( { contentSize : e.currentTarget.value } );
    }
}